# Document of Arbor

## Processor MarlinArbor

### Build Links from Hits

+ HitsCleaning

    Currently no cleaning here.

+ BuildInitLink

    Build links according to the thresholds.

+ LinkClean

    Only keep the link with lowest turning angle

+ LinkIteration


+ HitsClassification

    Classify the hits into simple/star seed/joint, leaves and iso hit

+ BranchBuilding

### Build clusters from branch

## Processor BushConnect

### TrackSort

    Apply pre-selection on tracks and classify the tracks

### BushSelfMerge


### TagCore

    Produce "ChargedCore" and "NeutralCore" according to track-cluster matching

### ParticleReco

#### ClusterFlag

The definition of variables for muon seletion is as following

- EcalNHit : number of hits in ECal
- HcalNHit : number of hits in HCal
- cluDepth : the distance between minpos and maxpos of the cluster.  How to understand the distance to surface.
- $FD_{all}$ : 
- $FD_{HCAL}$ :
- TrackEn : $E=p^2+m^2$, assuming the mass is the mass of Pion, 0.139 GeV
- avEnDisHtoL : energy-weighted distance $\sum_{hit} E*d/\sum E$. $d=|\vec{OA}\times\vec{AH}|$, O is the barycenter of the cluster, A is the point with least distance to calorimter surface, H is the given hit. This variable indicates the lateral width of the cluster.
- $rms_{Hcal}$ : RMS of the hit size in HCal?
- maxDisHel : maximum distance between hit and helix



The "muon" selection on track-cluster is as following

- cutmu1 : EcalNHit+2.*HcalNHit < 500
- cutmu2 : ( cluDepth>1400 ) || ( 1100<cluDepth<1400 && $FD_{all}$<0.5/20*(cluDepth-1000)$^2$)
- cutmu3 : 
    + if TrackEn > 70, cutmu3 = $FD_{all}$ < (600./avEnDisHtoL + 20)/100
    + else if TrackEn > 10, cutmu3 = $FD_{all}$ < (600./avEnDisHtoL -10 + 0.5*TrackEn)/100.
    + else if TrackEn > 7.5, cutmu3 = $FD_{all}$ < (600./avEnDisHtoL -10)/100.;
    + else cutmu3 = 1

- cutmu3b : 
    + if TrackEn > 10, cutmu3b = avEnDisHtoL < 25
    + else if TrackEn > 4.5, cutmu3b = avEnDisHtoL < 25 + 10 * (10 - TrackEn)
    + else cutmu3b=1
- cutmu10en : cutmu1 && cutmu2 && cutmu3 && cutmu3b
- cutmu4 : $FD_{HCAL}\ge0$
- cutmu5 : cluDepth>750-20/TrackEn
- cutmu6 : cluDepth>1200
- cutmu7 : $FD_{all}$<0.3/20*$\sqrt{clutDepth-1200}$
- cutmu8 : $rms_{Hcal}$ < 10 && $FD_{HCAL}$ < -0.25/600.*MaxDisHel+0.25
- cutmu9 : $FD_{all}$ < 0.35/20*$\sqrt{400-MaxDisHel}$ && MaxDisHel < 400 && EcalNHit+2.*HcalNHit > 85

- cutmu : 
    + if TrackEn > 9.5, cutmu=cutmu10en
    + else if TrackEn < 1.5, cutmu=cut5
    + else if TrackEn < 3.5, cutmu=(cutmu4 && cutmu6 && cutmu7)
    + else cutmu=cutmu3b && cutmu4 && cutmu6 && cutmu7 && cutmu8 && cutmu9

The definitions of variables for electron selection are as following:

- dEdx
- $FD_{ECALF10}$
- $FD_{ECAL}$
- SDTheta : the standard deviation of theta of cluster hits. It indicates the width of the cluster.


The "electron" selection on track-cluster is as following
- cute2 : (dEdx > 0.17e-6 && dEdx < 0.3e-6)||dEdx==0
- cute3 : $FD_{all}$ > 0.9*sin(cluDepth*1.57/800.) && cluDepth < 800- cute4 : $FD_{ECALF10}$ > 0.9*sin((cluDepth-200)*1.57/600.)
- cute5 : $EEClu_r$/TrackEn > 0.8 * sin((avEnDisHtoL-15)*1.57/20.) && avEnDisHtoL < 35
- cute6 : $FD_{ECAL}$ > 0.2 * log10(EcalNHit) && log10(EcalNHit) > 1.5
- cute7 : $FD_{all}$ >= 0.6/1.5*(log10(EcalNHit+2.*HcalNHit)-1.2-0.4*TrackEn/100.)
- cute8 : SDTheta < 0.012/1.5*(log10(EcalNHit+2.*HcalNHit)-1)
- cute :
    + if TrackEn<1.5, cute=cute2
    + else cute=cute2 && cute3 && cute4 && cute5 && cute6 && cute7 && cute8
    
If the cluster does not pass "cute" or "cutmu", it will be treated as "HAD" cluster(ClusterID is 211 or 212).


### Processor LICH

MVA based classifier, variables : 

+ EcalNHit
+ HcalNHit
+ NLEcal
+ NLHcal
+ maxDisHtoL
+ avDisHtoL
+ EcalEn/EClu
+ graDepth
+ cluDepth
+ minDepth
+ MaxDisHel
+ $FD_{all}$
+ $FD_{ECAL}$
+ $FD_{HCAL}$
+ $EEClu_{L10}$/EcalEn
+ $rms_{Hcal}$
+ $av_{NHH}$
+ $AL_{Ecal}$
+ $FD_{ECALF10}$
+ $FD_{ECALL20}$
+ $NH_{ECALF10}$
+ dEdx


## Useful links

+ [Arbor, a new approach of the Particle Flow Algorithm, CHEP2013](https://arxiv.org/pdf/1403.4784.pdf)
+ [Reconstruction of physics objects at the Circular Electron Positron Collider with Arbor, Eur.Phys.J.C 78 (2018) 5, 426](https://arxiv.org/pdf/1806.04879.pdf)
+ [ArborTracking: a tree topology track pattern recognition algorithm, Rad.Det.Tech.Meth. 4 (2020) 3, 377-382](https://doi.org/10.1007/s41605-020-00194-w)
+ [Lepton identification at particle flow oriented detector for the future $e^{+}e^{-}$ Higgs factories](https://arxiv.org/pdf/1701.07542.pdf)
+ [Fractal Dimension of Particle Showers Measured in a Highly Granular Calorimeter](https://arxiv.org/pdf/1312.7662.pdf)

## Questions

1, What does DisSeedSurface() mean? What distance is it?
2, The code of "rms_Hcal" in LICH seems wrong.
3, The index of TMVA reader in LICH seems wrong.

```C
for(int i=0;i<NEn;i++){
	if(TrackEn>=inputLevel[i]&&TrackEn<inputLevel[i+1]){
	reader_1=reader_all[i];
	for(int j=0;j<NPos-1;j++){
		if(abs(cosTheta)>=inputsubPos[j]&&abs(cosTheta)<inputsubPos[j+1]){
			reader=reader_1[j];
			}
		else{
			reader=reader_1[2];
			}
	}
  }
}
```
4, The currect BDT cut is :
- prob(e)>0.33&&prob(mu)<0.33&&prob(pi)<0.33, electron ID is assigned
- prob(e)<0.33&&prob(mu)>0.33&&prob(pi)<0.33, muon ID is assigned
- prob(e)<0.33&&prob(mu)<0.33&&prob(pi)>0.33, pion ID is assigned
    The cut ignores some cases like "prob(e)=0.3665596,prob(mu)=0.0444407,prob(pi)=0.5889995", in which the RECO PID is assigned as "0".
